# from typing import Any
from typing import Union
import typer
import os
import json
import csv
from pathlib import Path


def _check_source_file(source_file: str) -> bool:
    if os.path.isfile(source_file):
        return True
    else:
        return False


def _parse_value(value: str) -> Union[int, str, float, bool]:
    try:
        parsed_value = eval(value)
        parsed_value_type = type(parsed_value)
        if parsed_value_type not in [int, str, float, bool]:
            raise ValueError(f"Type {parsed_value_type} not supported")
        return parsed_value
    # If we want to enable all datatypes add ValueError to except
    except (NameError, SyntaxError):
        return str(value)


def _validate_modifier(modifier: str) -> bool:
    return modifier in ["set", "add", "remove", "inc"]


app = typer.Typer()


# TODO: Add parameter "modifier" by default "set"
# possible modifiers:
# https://solr.apache.org/guide/8_11/updating-parts-of-documents.html#atomic-updates
@app.command()
def single_field(
        source_file_path: Path,
        destination_path: Path,
        field: str,
        value,
        modifier: str = 'set',
        num_docs: int = 20000):
    print(f"Execution: {source_file_path}")
    data_list = []

    with open(source_file_path, 'r') as ids_file:
        for accession_id in ids_file:
            data = {
                "entryId": f"AF-{accession_id.strip()}-F1",
                f"{field}": {f"{modifier}": _parse_value(value)}
            }
            data_list.append(data)

    with open(destination_path, 'w') as result_file:
        json.dump(data_list, result_file)
        result_file.flush()


@app.command()
def multiple_fields(source_file_path: Path,
                    destination_path: Path,
                    modifier: str = 'set',
                    num_docs: int = 20000):
    if not source_file_path.is_file():
        raise FileNotFoundError(f"Source file {source_file_path} not found.")
    if not destination_path.is_dir():
        raise NotADirectoryError(f"Destination directory {destination_path} not found.")

    # TODO: check file and folder
    with open(source_file_path, 'r') as f:

        csv_reader = csv.DictReader(f)

        # get the csv headers
        headers = csv_reader.fieldnames

        # TODO: control this length to generate num_docs files
        data_list = []

        for row in csv_reader:
            data_dict = {}
            for header in headers:
                if header in ["uniprotAccession", "entryId"]:
                    data_dict[header] = row[header]
                else:
                    data_dict[header] = {f"{modifier}": _parse_value(row[header])}
            data_list.append(data_dict)
    destination_file = os.path.join(destination_path, "result_data_01.json")
    with open(destination_file, 'w') as result_file:
        json.dump(data_list, result_file)


if __name__ == '__main__':
    app()

